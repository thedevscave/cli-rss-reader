# CLI RSS reader  
Hi, this is a full code of the simplest CLI RSS reader written in Python  

The code was written for educational purposes specifically for the article about RSS and how it works. You can find this article here - https://thedevscave.com/en/posts/why_you_need_rss_and_how_it_works.html  

## How to run it  
The repo contains file `requirements.txt` with requirements needed to run the application. So to run the application follow next steps:  
1. Create a virtual environment - `python -m venv myenv`  
2. Activate venv. If you are on linux-based system - `. myenv/bin/activate`, if you are on Windows - `myenv/Scripts/activate` or `myenv/activate` depedending on your Python version  
3. Install dependencies - `pip install -r requirements.txt`  
4. Run the program - `python main.py https://thedevscave.com/en/rss`  

## Example of program output  
![Program output](./cli_rss_reader_output.png)
