import requests
from bs4 import BeautifulSoup
import datetime
import dateutil
import os
import webbrowser
import sys
from fake_useragent import UserAgent

class Post:
    def __init__(self):
        self.title = ""
        self.description = ""
        self.link = ""
        self.pubDate = ""
        self.language = ""

class Channel:
    def __init__(self):
        self.title = ""
        self.description = ""
        self.link = ""
        self.posts = []

class COLORS:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    RESET = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def bold(msg: str):
    return COLORS.BOLD + msg + COLORS.RESET

def underline(msg: str):
    return COLORS.UNDERLINE + msg + COLORS.RESET

def yellow(msg: str):
    return COLORS.YELLOW + msg + COLORS.RESET

def red(msg: str):
    return COLORS.RED + msg + COLORS.RESET

def blue(msg: str):
    return COLORS.BLUE + msg + COLORS.RESET

def green(msg: str):
    return COLORS.GREEN + msg + COLORS.RESET

def cyan(msg: str):
    return COLORS.BLUE + msg + COLORS.RESET

def get_file(url: str):
    ua = UserAgent()
    headers = { 'User-Agent': ua.random }
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        print(red("Failed to retrieve rss feed from url: {}".format(r.status_code)))
        return None
    return r.content

def parse_feed(feed: str):
    channel = Channel()
    try:
        soup = BeautifulSoup(feed, "xml")
        rss_tag = soup.find("rss")
        if rss_tag is None:
            print(red("No RSS tag was found"))
            return channel

        channel_tag = soup.find("channel")

        for channel_tag in channel_tag.children:
            if channel_tag.name == "title" and len(channel_tag.text) != 0:
                channel.title = channel_tag.text
            if channel_tag.name == "description" and len(channel_tag.text) != 0:
                channel.description = channel_tag.text
            if channel_tag.name == "link" and len(channel_tag.text) != 0:
                channel.link = channel_tag.text
            if channel_tag.name == "item" and len(channel_tag.text) != 0:
                post = Post()
                for item_tag in channel_tag:
                    if item_tag.name == "title" and len(item_tag.text) != 0:
                        post.title = item_tag.text
                    if item_tag.name == "description" and len(item_tag.text) != 0:
                        post.description = item_tag.text.replace("\n", "").replace("\r", "")
                    if item_tag.name == "pubDate" and len(item_tag.text) != 0:
                        post.pubDate = item_tag.text
                    if item_tag.name == "language" and len(item_tag.text) != 0:
                        post.language = item_tag.text
                    if item_tag.name == "link" and len(item_tag.text) != 0:
                        post.link = item_tag.text
                channel.posts.append(post)
    except AttributeError as err:
        print(red("Invalid RSS URL were provided"))
    return channel

def trim_and_fill(text: str, length: int, filler: str = " "):
    if len(text) <= length:
        result = text[:length].strip()
        for i in range(len(result), length):
            result += filler
        return result

    RFILL_NUM = 3
    result = text[:length - RFILL_NUM].strip()
    for i in range(len(result), length):
        result += '.'
    return result

def show_channel(channel: Channel):
    term_w, term_h = os.get_terminal_size()
    print(bold(yellow(channel.title)) + " [" + channel.link + "]")
    print(channel.description)

    print("")
    if len(channel.posts) < 1:
        print(red("No posts!"))
        return

    post_num = 1

    MAX_TITLE_LEN = 50
    TITLE_LEN = 0
    for post in channel.posts:
        if len(post.title) > TITLE_LEN:
            TITLE_LEN = len(post.title)

    if TITLE_LEN > MAX_TITLE_LEN:
        TITLE_LEN = MAX_TITLE_LEN

    print("Posts:")
    for post in channel.posts:
        x = dateutil.parser.parse(post.pubDate)
        pubDate = x.strftime("%b %d, %Y")
        COLORED_NUM_LEN = 12 # len('000') + len("ansii escape code for color")
        LANG_LEN = 2
        DATE_LEN = len(pubDate)
        MAX_DESCRIPTION_LEN = term_w - COLORED_NUM_LEN - LANG_LEN - TITLE_LEN - DATE_LEN

        # formatting data
        num = blue(str(post_num))
        title = trim_and_fill(post.title, TITLE_LEN)
        description = trim_and_fill(post.description, MAX_DESCRIPTION_LEN)

        print('{0:3} [{1:2}] {2} {3} {4}'.format(num.rjust(COLORED_NUM_LEN), post.language.ljust(LANG_LEN), title, description, pubDate.rjust(DATE_LEN)))
        post_num += 1


def main():
    if len(sys.argv) < 2:
        print(red("No URL were provided, shutting down."))
        print(f"Usage: python {sys.argv[0]} rss_url")
        print(f"Example: python {sys.argv[0]} https://thedevscave.com/en/rss")
        exit()

    feed_str = get_file(sys.argv[1])
    if feed_str is None:
        exit()
    channel = parse_feed(feed_str)
    if channel.title == "":
        exit()
    show_channel(channel)
    posts_num = len(channel.posts)
    if posts_num < 1:
        exit()
    print("")
    while True:
        try:
            num = int(input('Open the article number [1-{0}]: '.format(posts_num)))
            if num < 1 or num > posts_num:
                raise ValueError
        except ValueError as err:
            print("Input value from 1 to {0} please".format(posts_num))
            continue
        except KeyboardInterrupt as err:
            print("")
            exit()
        webbrowser.open(channel.posts[num-1].link, new=0, autoraise=True)
        exit()

if __name__ == '__main__':
    main()
